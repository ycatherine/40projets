var elixir = require('laravel-elixir');

/**
 * Overrides Elixir's default config.
 */
elixir.config.sourcemaps = false;
elixir.config.assetsPath = 'assets';
elixir.config.publicPath = '';

elixir(function(mix) {

    // Compile sass.
    mix.sass('app.scss', 'assets/css/app.css');

    mix.copy('node_modules/font-awesome/fonts', 'fonts/icons/font-awesome');

    mix.styles([
        'assets/css/app.css'
    ], 'css/app.css');


    mix.scripts([
        '../../node_modules/jquery/dist/jquery.js',
        'assets/js/app.js'
    ], 'js/app.js');


    mix.version(['css/app.css', 'js/app.js']);
});
